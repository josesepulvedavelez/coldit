﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
using System.Data.SqlClient; 

namespace Datos
{
    public class ProductoDAL
    {
        SqlConnection conexion;
        SqlDataReader lector;
        SqlCommand comando;
        int res; 

        public List<ProductoENT> GetProductos()
        {
            List<ProductoENT> lstProducto = new List<ProductoENT>();

            using (conexion = new SqlConnection(Conexion.Conectar()))
            {
                conexion.Open();

                using (comando = new SqlCommand("select * from producto", conexion))
                {
                    lector = comando.ExecuteReader();

                    while (lector.Read())
                    {
                        ProductoENT productoENT = new ProductoENT()
                        {
                            Codigo = Convert.ToString(lector["Codigo"]),
                            Valor = Convert.ToDouble(lector["Valor"]),
                            Nombre = Convert.ToString(lector["Nombre"]),
                            Id = Convert.ToInt32(lector["Id"])
                        };
                        lstProducto.Add(productoENT);
                    }
                }
            }
            return lstProducto;
        }

        public int InsertProducto(ProductoENT productoENT)
        {
            using (conexion = new SqlConnection(Conexion.Conectar()))
            {
                using (comando = new SqlCommand("insert into producto(codigo, valor, nombre) values(@codigo, @valor, @nombre)", conexion))
                {
                    comando.Parameters.AddWithValue("@codigo", productoENT.Codigo);
                    comando.Parameters.AddWithValue("@valor", productoENT.Valor);
                    comando.Parameters.AddWithValue("@nombre", productoENT.Nombre);

                    conexion.Open();
                    res = comando.ExecuteNonQuery();
                    conexion.Close();
                }
            }
            return res;
        }

        public int UpdateProducto(ProductoENT productoENT)
        {
            using (conexion = new SqlConnection(Conexion.Conectar()))
            {
                using (comando = new SqlCommand("update producto set codigo=@codigo, valor=@valor, nombre=@nombre where id=@id", conexion))
                {
                    comando.Parameters.AddWithValue("@codigo", productoENT.Codigo);
                    comando.Parameters.AddWithValue("@valor", productoENT.Valor);
                    comando.Parameters.AddWithValue("@nombre", productoENT.Nombre);
                    comando.Parameters.AddWithValue("@id", productoENT.Id);

                    conexion.Open();
                    res = comando.ExecuteNonQuery();
                    conexion.Close();
                }
            }
            return res;
        }

        public int DeleteProducto(ProductoENT productoENT)
        {
            using (conexion = new SqlConnection(Conexion.Conectar()))
            {
                using (comando = new SqlCommand("delete from producto where id=@id", conexion))
                {
                    comando.Parameters.AddWithValue("@id", productoENT.Id);

                    conexion.Open();
                    res = comando.ExecuteNonQuery();
                    conexion.Close();
                }
            }
            return res;
        }

    }
}
