﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using Entidades; 

namespace Datos
{
    public class FacturaDAL
    {
        SqlConnection conexion;
        SqlCommand comando;
        SqlDataReader lector;
        int max;
        int res;

        public void Insertar(FacturaENT facturaENT, List<ItemENT> lstItemENT)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (conexion = new SqlConnection(Conexion.Conectar()))
                    {
                        comando = new SqlCommand("insert into Factura(Cliente, ValorTotal) values(@Cliente, @ValorTotal)", conexion);
                        comando.Parameters.AddWithValue("@Cliente", facturaENT.Cliente);
                        comando.Parameters.AddWithValue("@ValorTotal", facturaENT.ValorTotal);

                        conexion.Open();
                        res = comando.ExecuteNonQuery();

                        comando = new SqlCommand("select @@IDENTITY", conexion);
                        max = Convert.ToInt16(comando.ExecuteScalar());

                        comando = new SqlCommand("insert into Item(Cantidad, Valor, ValorTotal, ProductoId, FacturaId) values(@Cantidad, @Valor, @ValorTotal, @ProductoId, @FacturaId)", conexion);

                        foreach (var item in lstItemENT)
                        {
                            item.FacturaId = max;

                            comando.Parameters.Clear();
                            comando.Parameters.AddWithValue("@Cantidad", item.Cantidad);
                            comando.Parameters.AddWithValue("@Valor", item.Valor);
                            comando.Parameters.AddWithValue("@ValorTotal", item.ValorTotal);
                            comando.Parameters.AddWithValue("@ProductoId", item.ProductoId);
                            comando.Parameters.AddWithValue("@FacturaId", item.FacturaId);
                            res = comando.ExecuteNonQuery();
                        }

                        conexion.Close();
                    }

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0}" + ex.Message);
            }
        }
    }
}
