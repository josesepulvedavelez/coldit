﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Entidades;
using Negocio; 

namespace Servicio.Controllers
{
    public class ProductoController : ApiController
    {
        ProductoBLL productoBLL = new ProductoBLL();

        [HttpGet]
        public IEnumerable<ProductoENT> ObtenerProductos()
        {
            return productoBLL.GetProductos();   
        }

        [HttpPost]
        public IHttpActionResult AgregarProducto([FromBody] ProductoENT productoENT)
        {
            if (ModelState.IsValid)
            {
                productoBLL.InsertProducto(productoENT);

                return Ok(productoENT);
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut]
        public IHttpActionResult ActualizarProducto([FromBody] ProductoENT productoENT)
        {
            if (ModelState.IsValid)
            {
                productoBLL.UpdateProducto(productoENT);

                return Ok(productoENT);
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut]
        public IHttpActionResult EliminarrProducto([FromBody] ProductoENT productoENT)
        {
            if (ModelState.IsValid)
            {
                productoBLL.DeleteProducto(productoENT);

                return Ok(productoENT);
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
