﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using Entidades;
using Negocio; 

namespace Servicio.Controllers
{
    public class HomeProductoController : Controller
    {
        string baseUrl = "http://localhost:59147/";

        public async Task<ActionResult> Index()
        {
            List<ProductoENT> lstRegistro = new List<ProductoENT>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage res = await client.GetAsync("api/producto/");

                if (res.IsSuccessStatusCode)
                {
                    var resResponse = res.Content.ReadAsStringAsync().Result;

                    lstRegistro = JsonConvert.DeserializeObject<List<ProductoENT>>(resResponse);
                }

                return View(lstRegistro);
            }
        }

        public ActionResult Crear()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Crear(ProductoENT productoENT)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59147/api/producto");
                var postTask = client.PostAsJsonAsync<ProductoENT>("producto", productoENT);
                postTask.Wait();

                var result = postTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    return Redirect("Index");
                }
            }

            ModelState.AddModelError(string.Empty, "Error contacte con el administrador de la app");
            return View("Index");
        }

        public ActionResult Editar(int Id)
        {
            ProductoENT productos = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);

                var responseTask = client.GetAsync("api/producto/" + Id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<ProductoENT>();
                    readTask.Wait();
                    productos = readTask.Result;
                }
            }
            return View(productos);
        }

        [HttpPost]
        public ActionResult Editar(ProductoENT productoENT)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                var putTask = client.PutAsJsonAsync($"api/producto/{productoENT.Id.ToString()}", productoENT);
                putTask.Wait();

                var result = putTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(productoENT);
        }

        public ActionResult Eliminar(int Id)
        {
            ProductoENT productos = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);

                var responseTask = client.GetAsync("api/producto" + Id.ToString());
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<ProductoENT>();
                    readTask.Wait();
                    productos = readTask.Result;
                }
            }
            return View(productos);
        }

        [HttpPost]
        public ActionResult Eliminar(ProductoENT productos, int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                var deleteTask = client.DeleteAsync($"api/producto/" + id.ToString());
                deleteTask.Wait();
                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(productos);
        }
    }
}
