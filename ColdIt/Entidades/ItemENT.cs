﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class ItemENT
    {
        public int Cantidad { get; set; }
        public double Valor { get; set; }
        public double ValorTotal { get; set; }
        
        public int ProductoId { get; set; }
        public int FacturaId { get; set; }
        public int Id { get; set; }
    }
}
