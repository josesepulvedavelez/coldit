﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class FacturaENT
    {
        public string Cliente { get; set; }
        public double ValorTotal { get; set; }
        public int Id { get; set; }
    }
}
