﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos; 

namespace Negocio
{
    public class ProductoBLL
    {
        List<ProductoENT> lstProducto;
        ProductoDAL productoDAL;
        int res; 

        public List<ProductoENT> GetProductos()
        {
            productoDAL = new ProductoDAL();
            lstProducto = productoDAL.GetProductos();

            return lstProducto;
        }

        public int InsertProducto(ProductoENT productoENT)
        {
            productoDAL = new ProductoDAL();
            res = productoDAL.InsertProducto(productoENT);

            return res;
        }

        public int UpdateProducto(ProductoENT productoENT)
        {
            productoDAL = new ProductoDAL();
            res = productoDAL.UpdateProducto(productoENT);

            return res;
        }

        public int DeleteProducto(ProductoENT productoENT)
        {
            productoDAL = new ProductoDAL();
            res = productoDAL.DeleteProducto(productoENT);

            return res;
        }
    }
}
