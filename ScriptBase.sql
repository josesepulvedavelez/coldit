create database ColdIt;
use ColdIt; 

create table Producto
(
	Codigo varchar(10),
	Valor float, 
	Nombre varchar(100),

	Id int identity(1, 1) primary key
); 

create table Factura
(
	Cliente varchar(100), 
	ValorTotal float,
	
	Id int identity(1, 1) primary key
);

create table Item
(
	Cantidad int, 
	Valor float, 
	ValorTotal float, 
	
	ProductoId int foreign key references Producto(Id),
	FacturaId int foreign key references Factura(Id)	
);

alter table Item add Id int identity(1, 1) primary key